var $_Config = function(){
    //项目js文件所在的域名
    var url=window.location.href;
    console.log(url);
    var _baseUrl;
    var _vendors_url;

    check();
    function check(){
        _baseUrl = 'file:///E:/Code/adk/src/js/federal';
        _vendors_url = 'file:///E:/Code/adk/src/federal/js/vendors';
    }
    return {
        //加载配置
        require_config:{
            baseUrl:_baseUrl,   //设置静态资源代码域名
            paths:{
                /*"jquery-2":["common/jquery/jquery-1.11.3/jquery"]  //自己加载的外部程序*/
            },
            vendors_url:_vendors_url
        },
        //路由配置--配置不同的请求地址加载不同的js文件，尽量一个请求文件加载一个js文件。
        route_config:{
            '*':_baseUrl+'js/common.js',
            '/':_baseUrl+'js/index.js',
            '/latest/': _baseUrl+ 'js/latest.js',
            '/anIosIndex/':_baseUrl + 'js/anIosIndex.js',
            '/classificationMap/':_baseUrl + 'js/classificationMap.js',
            '/softDetail/': _baseUrl + 'js/softDetail.js',
            '/zhuantiList/': _baseUrl + 'js/zhuantiList.js',
            '/PCsoftDetail/': _baseUrl + 'js/PCsoftDetail.js',
            '/newsIndex/':_baseUrl + 'js/newsIndex.js',
            '/newsList/':_baseUrl + 'js/newsList.js',
            '/newsDetail/':_baseUrl + 'js/newsDetail.js',
            '/zhuantiDetail/': _baseUrl + 'js/zhuantiDetail.js',
            '/categoryList/': _baseUrl + 'js/categoryList.js'
//          '/search/*':_baseUrl+"js/search.js",
//
//          '/goods/*':_baseUrl+"js/goods.js",
//          '/99baoyou/*':_baseUrl+"js/single_99.js",
//          '/article/*':_baseUrl+"js/article.js",
//          '/zt/':_baseUrl+"js/zt.js",
//          '/column/*':_baseUrl+"js/column.js",
//          '/Waterfall/':_baseUrl+"js/Waterfall.js",
//			'/choosePhoto/':_baseUrl+"js/choosePhoto.js",
//          '/chosePic/': _baseUrl + "js/chosePicture.js"
        }
    };
};
