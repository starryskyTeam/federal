$(function() {
	$(".tabs-nav-top .item-nav").click(function() {
		var selector = $(this).attr('data-toggle');
		var $active = $(this).parents('.tabs-nav-top').find('.active');
		$active.removeClass('active');
		$(this).addClass('active');
		$(this).parents('.tabs').find('.tabs-contents').find('.tab-content').removeClass('active');
		$("#item-"+ selector).addClass('active');
	})

})