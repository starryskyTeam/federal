let fs = require("fs");

module.exports = {
    "temp":function (tagString,tags) {
        let filename = tags.pop();
        let tempPath = fs.realpathSync(this.option.baseDir+"/tempvar/"+filename+this.option.ext);
        let the = this;

        if(this._env =='template') return false;

        return new Promise(function (resolve, reject) {
            fs.readFile(tempPath, {flag: 'r', encoding: 'utf8'},(err,data)=>{
                if (err) {
                    reject(err);
                }else{
                    let compile = the.createChildCompile();
                    let  ret = compile.compile(data);
                    let  v = {};

                    if(ret instanceof Promise){
                        v[tagString] = new Promise(function (resolve, reject) {
                             ret.then(function (datas) {
                                 let html = compile.replace(data,datas);
                                 resolve(html);
                             }).catch(function (err) {
                                 reject(err);
                             })
                        });
                        resolve(v);
                    }else if(ret === false){
                        v[tagString] = data;
                    }else{
                        let v = {};
                        v[tagString] = ret;
                    }
                    resolve(v);

                }
            })
        });

    },
    'public_r':function (tagString, tags) {
        let key = tags.pop();
        if(this._env =='template'){
            return "<?php echo $public_r['"+key+"']; ?>";
        }

        if(typeof(this._template.public_r[key])!= undefined){
           return this._template.public_r[key];
        }

        return false;
    }
}