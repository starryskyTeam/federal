let path = require('path');

let compile;
function MeTestPlugin (option) {
    this.option = option;
    compile  = new (require("./compile"))(option);
}

MeTestPlugin.prototype.apply = function (compiler) {

    compiler.plugin('compilation', function(compilation) {
        //console.log('The compiler is starting a new compilation...');

        compilation.plugin('html-webpack-plugin-before-html-processing', function(htmlPluginData, callback) {
            //htmlPluginData.html += 'The magic footer';
            //console.log(htmlPluginData.html);

            let html =  htmlPluginData.html;
            let compilePromise =  compile.compile(html);

            if(compilePromise == false){
                callback(null, htmlPluginData);
                return;
            }

            compilePromise.then(function(datas){

                let p = compile.replace(htmlPluginData.html,datas);
                p.then(function (html) {
                    htmlPluginData.html = html;
                    callback(null, htmlPluginData);
                })

            }).catch(function (error) {

            })

        });
    });
}

module.exports = MeTestPlugin;