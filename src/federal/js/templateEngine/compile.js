let process = require("./translate");

function Parse(option) {
    this.option = option;
    this._env = option.env?option.env:"page";
    this._template = option.template?option.template:{};

    this.register(process);
}

Parse.prototype._tags = {};
Parse.prototype.register = function (tags) {
    let the = this;
    Object.keys(tags).forEach(function (tag) {
        the._tags[tag] = tags[tag];
    })
};

Parse.prototype.createChildCompile = function () {
    return new Parse(this.option);
}

Parse.prototype.compile= function (html) {

    let include = /\[!--(.*)--\]/g
    let ret  =  html.match(include);
    let results = [];
    let the = this;

    if(ret){
        ret.forEach(function (k, v) {
            let leftSub =k.substring(4);
            let tag = leftSub.substring(0,leftSub.length-3);
            let content = the.run(tag);
            results.push(content);
        })
    }
    
    if(results.length == 0){
        return false;
    }else{
        return Promise.all(results);
    }
}

Parse.prototype.run = function (tagString) {
    let tags = tagString.split(".");
    let the = this;
    let res = false;

    //注册函数处理
    Object.keys(this._tags).forEach(function (tag) {
        if(tagString.indexOf(tag) === 0){
            res =  the._tags[tag].apply(the,[tagString,tags]);
        }
    });

    if(res === false){
        let rep = this._template[tagString];
        if(rep){
            if(rep instanceof Function){
                res = this._template[tagString].apply(this);
            }else{
                res = this._template[tagString];
            }
        }
    }

    if(res instanceof Promise){
        return res
    }else{
        return new Promise(function (resolve, reject) {
            let v = {};
            v[tagString] = res;
            resolve(v);
        })
    }
};

/**
 *
 * @param html
 * @param datas
 * @returns {*}
 */
Parse.prototype.replace = function (html,datas) {
    let rets = [];

    datas.forEach(function (v) {
        Object.keys(v).forEach(function (tag) {
            let key = "\\[!--"+tag+"--]";
            let ret = v[tag];

            let p = new Promise(function (resolve, reject) {
                if(ret instanceof  Promise){
                    ret.then(function (data) {
                        let r ={};
                        r[tag] = data;
                        resolve(r);
                    });
                }else{
                    if(ret === false){
                        resolve(false);
                    }else{
                        let r ={};
                        r[tag] = ret;
                        resolve(r);
                    }
                }
            });

            rets.push(p);
        })
    });

    if(rets.length>0){
        return Promise.all(rets).then(function (datas) {

            datas.forEach(function (v) {
                Object.keys(v).forEach(function (tag) {
                    let key = "\\[!--"+tag+"--]";
                    let ret = v[tag];
                    if(ret !== false){
                        html = html.replace(new RegExp(key,"gm"),ret);
                    }
                });
            });

            return html;
        });
    }

    return Promise.resolve(html);
}

parse = Parse;
module.exports = parse;