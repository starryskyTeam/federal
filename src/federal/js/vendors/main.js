/**
 * 路由系统
 */
define("route",['jquery'],function(){
    return {
        run:function(configs){
            if(typeof(configs) != 'object'){
                return false;
            }

            var route_require = [];
            for( config in configs){
                if(this.matching(config)){
                    route_require.push(configs[config]);
                }
            }

            if(route_require.length>0)require(route_require);
        },
        matching:function(arg){
            var body_pathname = $("body").data('pathname');
            if(!body_pathname){
                var pathname = window.location.pathname;
            }else{
                var pathname = body_pathname;
            }

            if(arg == '/' && (pathname == "/" || pathname=='/index2S.html' || pathname=='/index.php' ) ){ //首页匹配
                return true;
            }else if(arg == '*'){ //全局匹配
                return true;
            }else if(arg.indexOf("*") != -1){
                var reg = new RegExp(arg,"g");
                return reg.test(pathname);
            }else if(arg == pathname){
                return true;
            }
            return false;
        }
    };
});

/**
 * 配置和运行应用程序
 */
(function(config){
    window._sys_config = config;
    var require_config = config.require_config;
    var vendors_url = require_config.vendors_url;
    var paths = {
        "jquery":[
            vendors_url+"/jquery-1.10.2/jquery"
        ],
        'jquery-cookie':[
            vendors_url+"/jquery-cookie/jquery.cookie"
        ],
        'bootstrap':[
            vendors_url+"/bootstrap/js/bootstrap"
        ],
        'layer':[
            vendors_url+"/layer/layer"
        ],
        'swiper':[
            vendors_url+"/swiper/swiper.min"
        ],
        'jquery-soChange':[
            vendors_url+"/jquery-plugin/jquery.soChange"
        ],
        'distpicker':[
            vendors_url+"/distpicker/distpicker"
        ],
        'distpicker-data':[
            vendors_url+"/distpicker/distpicker.data"
        ]

    };
    var ex_paths =require_config.paths;
    for(path in ex_paths){
        paths[path] = ex_paths[path];
    }

    require.config({
        baseUrl:require_config.baseUrl,
        paths:paths,
        shim:{
            'jquery-cookie':['jquery'],
            'layer':['jquery'],
            'bootstrap':['jquery'],
            'jquery-soChange':['jquery'],
            'swiper':['jquery'],
            'distpicker':['distpicker-data']


        }
    });

    require(['route'],function(route){
        route.run(config.route_config);
    });



})($_Config());