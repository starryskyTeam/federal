
    window.itmeSlide=function(obj,minlen,ileftnum,num) {// 父元素，每屏几块，每次移动几块
        var ilen = $(obj+' .tab-wrap li').length;//obj父
        var nub = 0;
        var iwidth = $(obj+' .tab-wrap li').eq(0).outerWidth(true); //每块宽度
        var iminlen = ilen - minlen; //minlen是一屏包含几块
        var onetime = 500; //时间
        var ileft=ileftnum ? ileftnum*iwidth : iwidth;//每次滑多长，
        var mnum =ileftnum ? (num ? num : 1 ): iminlen;
        $(obj+' .tab-wrap ul').css('width', ilen * iwidth)
        $(obj+' .btn.next').on('click', function() {
            nub++;
            if (nub > mnum) {
                nub = 0;
            }
            $(obj+' .tab-wrap ul').stop().animate({
                    left: -ileft * nub
                },
                onetime);
        });
        $(obj+' .btn.prev').on('click', function() {
            nub--;
            if (nub < 0) {
                nub = mnum;
            }
            $(obj+' .tab-wrap ul').stop().animate({
                    left: -ileft * nub
                },
                onetime);
        })
    }
