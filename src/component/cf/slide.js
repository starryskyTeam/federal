function slid(obj, b) {
    this.obj = $(obj);
    this.wrap = this.obj.find('.slide-wp');
    this.num = 0;
    this.timer = this.timer2 = null;
    this.li = this.obj.find('li')
    this.len = this.li.length;
    this.pBtn = this.obj.find('.eprev');
    this.nBtn = this.obj.find('.enext');
    this.focuswp = this.obj.find('.slide-focus');
    this.iwidth = this.obj.width();
    this.iheight = this.obj.height();
    this.lifirst = this.obj.find('li').first();
    this.set = {
        speed: 500,
        type: 1,
        time: 1000,
        auto: true
    };
    if(b) $.extend(this.set, b);
    if(this.len == 1) return false;
    this.init();
}
slid.prototype = {
    init: function() {
        var _t = this;
        if(_t.set.type == 1) {
            this.wrap.append(this.lifirst.clone())
            this.li = this.obj.find('li')
            this.len = this.li.length;
            this.wrap.width(this.iwidth * this.len)
        } else {
            this.lifirst.css('opacity', 1)
        }
        this.li.width(this.iwidth);
        this.li.height(this.iheight);
        this.obj.on('mouseover', function() {
            _t.pBtn.show();
            _t.nBtn.show();
            clearInterval(_t.timer2)
        });
        this.obj.on('mouseout', function() {
            _t.pBtn.hide();
            _t.nBtn.hide();
            _t.set.auto && _t.start();
        })
        _t.pnclick();
        _t.set.focus && _t.focus();
        _t.set.auto && _t.start();

    },
    move: function(m) {
        this.num = m;
        // 左右
        if(this.set.type == 1) {
            if(m == this.len) {
                this.wrap.css({
                    left: 0
                });
                this.num = 1;
            }
            if(m == -1) {
                this.wrap.css({
                    left: -(this.len - 1) * this.iwidth
                });
                this.num = this.len - 2;
            };
            this.wrap.stop().animate({
                left: -this.num * this.iwidth
            }, 500);
            this.focuswp.find('span').removeClass('active')
            this.num == this.len - 1 ? this.focuswp.find('span').eq(0).addClass('active') : this.focuswp.find('span').eq(this.num).addClass('active');
        } else {
            // 透明
            if(m == this.len) {
                this.num = 0;
            }
            if(m == -1) {
                this.num = (this.len - 1);
            }
            this.li.animate({
                opacity: 0
            }, 500).removeClass('active');
            this.li.eq(this.num).stop().animate({
                opacity: 1
            }, 500).addClass('active');
            this.focuswp.find('span').removeClass('active')
            this.focuswp.find('span').eq(this.num).addClass('active');
        }
    },
    stop: function() {
        this.set.auto = false;
        clearInterval(this.timer2)
    },
    start: function() {
        var _t = this;
        _t.set.auto = true;
        clearInterval(_t.timer2);
        _t.timer2 = setInterval(function() {
            _t.next();
        }, _t.set.time)
    },
    pnclick: function() {
        var _t = this;
        this.pBtn.on('click', function() {
            _t.prev();
        })
        this.nBtn.on('click', function() {
            _t.next();
        })
    },
    focus: function(m) {
        var _t = this;
        if(_t.set.type == 1) {
            for(var i = 0; i < _t.len - 1; i++) {
                _t.focuswp.append('<span></span>')
            }
        } else {
            for(var i = 0; i < _t.len; i++) {
                _t.focuswp.append('<span></span>')
            }
        }
        _t.focuswp.find('span').eq(0).addClass('active');
        _t.focuswp.find('span').on('click', function() {
            _t.focuswp.find('span').removeClass('active')
            _t.to($(this).index())
            $(this).addClass('active')
        })
    },
    to: function(n) {
        this.move(n);
    },
    prev: function() {
        this.num--;
        this.move(this.num)
    },
    next: function() {
        this.num++;
        this.move(this.num)
    },
    auto: function() {
        this.start()
    }
};
window.slid = slid;
