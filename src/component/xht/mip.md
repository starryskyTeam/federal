##1.提前引入mip
    <link rel="canonical" href="xxx.html" >
##2.在form表单中，添加一个class，class="search-software",翻译成标签&lt;mip-form&gt;&lt;/mip-form&gt;。form表单中的action,翻译成url
        <form class="search-software" action="www.xxxx">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="请输入关键字" id="keyword">
                <span class="input-group-addon" id="basic-addon">搜索</span>
            </div>
        </form>
        
    引入:<script src=" https://mipcache.bdstatic.com/static/v1/mip-form/mip-form.js"></script >
      
    
##3.在img标签中，添加一个class=“search-img”,这个class尽量不要写样式，另外img外层用div包裹,img同层级没有兄弟元素。翻译成 &lt;mip-img&gt; &lt;/mip-img&gt;
    <div>
       <img src="images/class-icon-1s.png" class="search-img"/>
    </div>
##4.tab切换，把class="mip-vd-tabs" 的div翻译成 &lt;mip-vd-tabs&gt; &lt;/mip-vd-tabs&gt;
    <div class="mip-vd-tabs">
          <section>
              <li>第一页</li>
              <li>第二页</li>
              <li>第三页</li>
              <li>第四页</li>
          </section>
          <div>内容1</div>
          <div>内容2</div>
          <div>内容3</div>
          <div>内容4</div>
    </div>
    
    引入:<script src="https://mipcache.bdstatic.com/static/v1/mip-vd-tabs/mip-vd-tabs.js"></script>

##5.轮播图建议用swiper，提前加上宽高等属性
    <div class="swiper-container"
       layout="responsive" 
       width="600" 
       height="400">
        <div  class="swiper-wrapper">
                <div  class="swiper-slide">
                       <a  href="http://www.baidu.com">
                           <img  src="images/ar1.jpg"/>
                        </a>
                </div>
                <div  class="swiper-slide">
                        <a  href="http://www.baidu.com">
                            <img  src="images/ar2.jpg"/>
                        </a>
                </div>
                <div  class="swiper-slide">
                        <a  href="http://www.baidu.com">
                            <img  src="images/ar1.jpg"/>
                        </a>
                </div>
        </div>
    </div>
##6.头部下拉，class="mip-365xiazai-header"的div 翻译为&lt;mip-365xiazai-header&gt;&lt;/mip-365xiazai-header&gt;
    <div class="mip-365xiazai-header">  
    
    </div>  
    引入:<script src="https://mipcache.bdstatic.com/extensions/platform/v1/mip-365xiazai-header/mip-365xiazai-header.js"></sctipt>
##7.头部显示和隐藏。class="mip-365xiazai-tabtap"的div 翻译为&lt;mip-365xiazai-tabtap&gt;&lt;/mip-365xiazai-tabtap&gt;
    <div class="mip-365xiazai-tabtap">  
    
    </div> 
    引入：<script src="https://mipcache.bdstatic.com/extensions/platform/v1/mip-365xiazai-tabtap/mip-365xiazai-tabtap.js"></script>
##8.极速下载和普通下载切换最外层，添加div,class=mip-huajun-downtag,在普通下载中添加class=“ptdownload”,极速下载添加class=“spdownload”
    <div class="mip-huajun-downtag">
        <div class="header-img">
            <mip-img src="images/xz.png" alt=""></mip-img>
        </div>
            <div class="content-middle">
                <h3>阴阳师最新官方版</h3>
                <p>大小:<span>660kb</span></p>
                <p>版本:<span>3.0.2</span></p>
            </div>
            <div class="content-bottom">
                <a href="https://www.baidu.com" target="_blank" class="ptdownload">普通下载</a>
                <a href="https://www.baidu.com" target="_blank" class="spdownload">极速下载</a>
            </div>
    </div>
    引入：<script src="https://mipcache.bdstatic.com/static/v1/mip-huajun-downtag/mip-huajun-downtag.js"></script>
##9.查看更多，div结构
    <div class="mip-down-hideshow" hsId="2">
       <div id="summary">
           一款经典的2D横版射击游戏不过添加了一些新功能，比如解谜元素。
       </div>
       <div id="details ">
          一款经典的2D横版射击游戏不过添加了一些新功能，比如解谜元素。
       </div>
       <div class="all"  >
          <span class="alls hideshow-btn arrowdown">查看全部</span>
       </div>
    </div>
    
    引入：<script src="https://mipcache.bdstatic.com/static/v1/mip-down-hideshow/mip-down-hideshow.js"></script>
    
##10. 百度统计(待用)class="mip-stats-baidu"
    <mip-stats-baidu token="02890d4a309827eb62bc3335b2b28f7f"></mip-stats-baidu>
    <script src="https://mipcache.bdstatic.com/static/v1/mip-stats-baidu/mip-stats-baidu.js"></script>
##11.百度分享（待用）class="mip-share"翻译
    <div class="mip-share-container">
      <mip-share></mip-share>
    </div>
    引入：<script src="https://mipcache.bdstatic.com/static/v1/mip-share/mip-share.js"></script>
##12.pcsoft专用搜索，class="mip-pcsoft-form"，翻译为&lt;mip-pcsoft-form&gt;&lt;/mip-pcsoft-form&gt;
    <div class="mip-pcsoft-form">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="请输入关键字" id="keyword">
            <span class="input-group-addon" id="basic-addon">搜索</span>
        </div>
    </div>
    引入：<script src="https://mipcache.bdstatic.com/extensions/platform/v1/mip-pcsoft-form/mip-pcsoft-form.js"></script>
##13.好玩和不好玩。
     删除class="smile-click"的onclick事件属性。删除class="get-click"的文本内容的script标签，同时获取其data-click属性值填充为文本内容。
     
    <div class="icon pull-left">
            <i class="iconfont icon1 smile-click" onclick="ding(<?=$classid?>,<?=$id?>,'top')" >&#xe607;</i>
            <span class="span1" >好玩</span>
            <span class="span2 get-click" data-click="[!--diggtop--]" id="digg">(<script src=<?=$public_r['search_url']?>/e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&down=5></script>)</span>
            <i class="iconfont icon2 smile-click" onclick="ding(<?=$classid?>,<?=$id?>,'down')">&#xe60e;</i>
            <span  class="span3">不好玩</span>
            <span  class="span4 get-click" id="bdigg" data-click="[!--diggdown--]">(<script src=<?=$public_r['search_url']?>/e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&down=6></script>)</span>
    </div>
 
#头部
    <link rel="stylesheet" type="text/css" href="https://mipcache.bdstatic.com/static/v1/mip.css" >

#尾部
    <script src="https://mipcache.bdstatic.com/static/v1/mip.js">
  </script >
  