$(function() {
	$("#goPage").on('click', function() {
		var _this = $(this), _input = $(this).parents('li').siblings().find('input'),reg = /^[0-9]*$/;
		var data_page, max_page = parseInt($(".pagination").find('li').eq(0).attr('data-max-page'));
		var val = _input.val();
		console.log(max_page);
		if(val.replace(/(^s*)|(s*$)/g, "").length == 0  || ! reg.test(val) || val > max_page || val <= 0) {
			_input.addClass('error-border');
		}else {
			if(val == 1) {
				data_page = $(".pagination").find('li').eq(0).attr('data-page').replace("-[!--page--]", "");
			}else {
				data_page = $(".pagination").find('li').eq(0).attr('data-page').replace("-[!--page--]", "-"+val);
			}
			console.log(data_page);
			window.location.href = data_page;
		}
		console.log(_input.val());
	})
	$(".pagination li input").on('click', function() {
		if($(this).hasClass('error-border')) {
			$(this).removeClass('error-border');
		}
	})

	function hidepage(){
		if($(".pagination li").length <= 2 ){
			console.log("页面小于2");
			$(".pagination li").hide();
		}
	}
	hidepage();
})

