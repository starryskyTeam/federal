
var Change = {
    now : 0,
    li_item : $(".click-wrap").find(".change-item"),
    li_length : $(".click-wrap").find(".change-item").length,
    change: function() {
        var that = this;
        that.now++
        if(that.now == that.li_length - 1) {
            that.now = -1;
        }
        that.li_item.hide();
        that.li_item.eq(that.now).show();
    },
    init: function() {
        var that = this;
        that.li_item.hide();
        that.li_item.eq(that.now).show();
    }
}
$(".click-change").click(function() {
    Change.change();
})
Change.init();
