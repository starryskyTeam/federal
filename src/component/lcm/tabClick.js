$(function() {
	$(".clickTabs-nav-top .nav").click(function() {
		var selector = $(this).attr('data-toggle');
		var $active = $(this).parents('.clickTabs-nav-top').find('.active');
		$active.removeClass('active');
		$(this).addClass('active');
		$(this).parents('.tabs-click').find('.tabs-contents').find('.tab-content').removeClass('active');
		$("#item-"+ selector).addClass('active');
	})

})