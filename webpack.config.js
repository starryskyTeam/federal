var webpack = require('webpack')


/**
 * css导出
 */
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');

var extractHtml = new ExtractTextPlugin('[name].html');
var extractStyles = new ExtractTextPlugin('[name].css');

var HtmlWebpackLayoutPlugin = require("html-webpack-layout-plugin");
var path = require('path');

module.exports = {

    entry: {
        index: [
            path.resolve(__dirname, 'src/templates/index.pug'),
        ],
        'css/application': [
            path.resolve(__dirname, 'src/188soft/application.scss')
        ]
    },
    output: {
        path: path.resolve(__dirname,"dist/"),
        filename: "[name].html"
    },
    plugins: [
        extractStyles,
        extractHtml
    ],
    module: {
        loaders: [
            { test: /\.css$/,
                loader: extractStyles.extract({ fallback: 'style-loader', use: 'css-loader' })
            },
            { test: /\.html$/, loader: 'html-loader' },
            {
                test: /\.scss$/,
                loader: extractStyles.extract({
                    loader: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
            {
                test: /\.pug$/,
                loader: extractHtml.extract({
                    loader: ['html-loader', 'pug-html-loader?pretty&exports=false']
                })
            }
            ]
    }
}