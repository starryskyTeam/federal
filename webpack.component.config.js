var webpack = require('webpack')

/**
 * css导出
 */
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');

var extractHtml = new ExtractTextPlugin('[name].html');
var extractStyles = new ExtractTextPlugin('[name].css');

var path = require('path');

let MeTestPlugin = require("./src/federal/js/templateEngine/webpackplugin");

let MeTestPluginObject = new MeTestPlugin({
    baseDir:path.resolve(__dirname, 'src/component/'),
    ext:".php",
    env:'page', //page,template
    template:{
        pagetitle:"网站名称",
        public_r:{
            search_url:'asd',
            add_web_url:'123'
        }
    }
});

module.exports = {
    entry: {
        'css/application': [
            path.resolve(__dirname, 'src/component/application.scss')
        ]
    },
    output: {
        path: path.resolve(__dirname,"dist/component"),
        filename: "[name].js"
    },
    plugins: [
        extractStyles,
        MeTestPluginObject,
        extractHtml,
        new HtmlWebpackPlugin({
            'filename':'layout.html',
            'template': path.resolve(__dirname, 'src/component/layout.ejs'),
            'chunks':[
                'css/application'
            ]
        })
    ],
    module: {
        loaders: [
            /*{
             test: /\.(png|jpg)$/,
             use: [
             "file-loader?"+fileLoaderConfig
             ]
             },*/
            {test: /\.ejs$/, loader: 'ejs-compiled-loader'},
            { test: /\.css$/,
                loader: extractStyles.extract({ fallback: 'style-loader', use: 'css-loader' })
            },
            { test: /\.html$/, loader: 'html-loader' },
            {
                test: /\.scss$/,
                loader: extractStyles.extract({
                    loader: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
            {
                test: /\.pug$/,
                loader: extractHtml.extract({
                    loader: ['html-loader', 'pug-html-loader?pretty&exports=false']
                })
            }
        ]
    }
}